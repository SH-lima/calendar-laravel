<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasFactory;

    public static function create($content, $date){

        DB::insert(" INSERT INTO event (content, date) VALUES (:content , :date) ", [
            "content" => $content,
            "date" => $date
        ]);
    }

    public static function updateContent($id, $content){
        DB::table('event')
            ->where('id', $id)
            ->update(['content' => $content]);
    }

    protected $table = 'event';

    protected $fillable = ['content', 'date'];
}
