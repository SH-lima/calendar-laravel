<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\GoogleCalendar\Event as GoogleEvent;

class EventController extends Controller
{
    public function newEvent(Request $request){
        $validated = $request->validate([
            "content" => "required",
            "date" => "required"
        ]);
        $content = $validated["content"];
        $date = $validated["date"];
        Event::create($content, $date);
        $dateFormat = explode("-", $date);

        $googleEvent = new GoogleEvent;
        $googleEvent->name = $content;
        $googleEvent->startDateTime = Carbon::createFromDate($dateFormat[0], $dateFormat[1], $dateFormat[2]);
        $googleEvent->endDateTime = Carbon::createFromDate($dateFormat[0], $dateFormat[1], $dateFormat[2]);

        $googleEvent->save();
        return redirect('/');
        exit;
    }

    public function readEvents(){
        $events = Event::all();
        return view('welcome',["events" => $events]);
    }
     
    public function delete(Request $request){
        $validated = $request->validate([
            "id" => "required"
        ]);
        $id = $validated["id"];
        $event = Event::find($id)
                ->delete();
        return redirect('/');
        exit;
    }

    public function update(Request $request){
        $validated = $request->validate([
            "id" => "required",
            "content"=>"required"
        ]);
        $id = $validated["id"];
        $content = $validated["content"];
        Event::updateContent($id, $content);
        return redirect('/');
        exit;
    }
       
}
