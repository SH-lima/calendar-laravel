<?php

// use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
use Spatie\GoogleCalendar\Event as GoogleEvent;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('/create', [EventController::class,'newEvent']);

Route::get('/', [EventController::class,'readEvents']);

Route::post('/delete', [EventController::class,'delete']);

Route::post('/update', [EventController::class,'update']);

// Route::get('/test', function () {

//     $e = GoogleEvent::get();
//     dd($e);
// });