selectYear = document.getElementById("year");
selectMonth = document.getElementById("month");
monthAndYear = document.getElementById("monthAndYear");
daysTable = document.querySelector("#days");
buttonNext = document.getElementById("next");
buttonPrevious = document.getElementById("previous");
editButton = document.getElementById("editButton");

today = new Date();
currentMonth = today.getMonth();
currentYear = today.getFullYear(); 

console.log(today, currentYear ,currentMonth )
months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];



function showCalendar(month, year) {
    
    let firstDay = (new Date(year, month)).getDay();
    
    daysTable.innerHTML = "";
    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;

    // get data from database
    // var events = {!! json_encode($events) !!}
       
    
    // display days of the month in table 
    daysInMonthVar = daysInMonth(month, year)
    for( let i = 1; i < daysInMonthVar+1; i++){
        let fullDate = year + "-" + monthFormat(month) + "-" + dayFormat(i);
        daysTable.insertAdjacentHTML('beforeend',`<li class="day">${i}</li>`);
        
        // display events in calendar
        let arrayNodes = daysTable.childNodes;
        for(let j = 0; j< arrayNodes.length ; j++){
            let element = arrayNodes[j]; 

            for(let x=0; x<events.length; x++){ 
                let event = events[x];
                let eventDateFormat = event.date.split("-");
                if(event.date == fullDate && parseInt(element.innerHTML) == parseInt(eventDateFormat[2])){
                    element.insertAdjacentHTML('beforeend',`<li class="eventsOfDay">
                    ${event.content}
                    <div class="eventButtons">
                    <form action="/delete" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="${event.id}">
                        <button><img src="https://img.icons8.com/fluency-systems-filled/22/000000/filled-trash.png"/></button>
                    </form>
                    </div>
                    </li>`);
                } 
            }  
        } 
    }
    
}
function editContent(id, event){
    
    console.log("editContent"+id)
}
function monthFormat(month){      
    let monthLength = month.toString().length
    if(monthLength !== 1){
        value =  month + 1;
        return value;
    }else{
        let value = "0"+(month+1)%12;
        return value;
    }
}
function dayFormat(day){      
    let dayLength = day.toString().length
    if(dayLength !== 1){
        return day;
    }else{
        let value = "0"+ day;
        return value;
    }
}
function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}

showCalendar(currentMonth, currentYear)

function next() {
    currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
    currentMonth = (currentMonth + 1) % 12;
    showCalendar(currentMonth, currentYear);
}

function previous() {
    currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
    currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
    showCalendar(currentMonth, currentYear);
}

buttonNext.addEventListener('click', ()=>{
    next()
})
buttonPrevious.addEventListener('click', ()=>{
    previous()
})
