<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="css/style.css"></link>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    </head>
    <body >
        <section class="card__header">
        <h3 id="day"></h3>
        <h3 id="month"></h3>
        <h3 id="year"></h3>
        </section>
        <form class="sizeForm " action="/create" method="POST">
            @csrf
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Ajouter un évenement</label>
                <input name="content" class="form-control form-control-lg" id="exampleFormControlTextarea1" rows="1"></input>
            </div> 
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Date</label>
                <input name="date" type="date" class="form-control form-control-lg" id="exampleFormControlInput1" >
            </div>
            <button class="mb-3">
                <img src="https://img.icons8.com/external-flatart-icons-outline-flatarticons/44/000000/external-send-basic-ui-elements-flatart-icons-outline-flatarticons.png"/>
            </button>
        </form> 
        <div class="choseDate center">
            <button id="previous"><img src="https://img.icons8.com/glyph-neue/44/000000/circled-chevron-left.png"/></button>
            <h2 id='monthAndYear'></h2> 
            <button id="next"><img src="https://img.icons8.com/glyph-neue/44/000000/circled-chevron-right.png"/></button>
        </div>
        
        
        <ul id="days" ></ul>
        
    <script src="js/scripts.js"></script>

    </body>
</html>
